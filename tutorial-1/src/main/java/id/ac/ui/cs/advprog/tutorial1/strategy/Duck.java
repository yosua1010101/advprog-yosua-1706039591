package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    // TODO Complete me!
    
    public abstract void display();

    public void swim() {

    }

    public void setFlyBehavior(FlyBehavior f) {
        this.flyBehavior = f;
    }

    public void setQuackBehavior(QuackBehavior q) {
        this.quackBehavior = q;
    }
}
