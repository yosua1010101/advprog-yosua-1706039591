package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.ListIterator;

public class MacroCommand implements Command {

    private List<Command> commands;

    public MacroCommand(Command[] commands) {
        this.commands = Arrays.asList(commands);
    }

    @Override
    public void execute() {
        // TODO Complete me!
        this.commands.stream().forEach(Command::execute);
    }

    @Override
    public void undo() {
        // TODO Complete me!
        List<Command> newCommand = reverse(this.commands);
        newCommand.stream().forEach(Command::undo);
    }

    List<Command> reverse(List<Command> commands) {
        List<Command> newCommand = new ArrayList<Command>();
        ListIterator<Command> li = commands.listIterator(commands.size());
        while (li.hasPrevious()) {
            newCommand.add(li.previous());
        }
        return newCommand;
    }
}
