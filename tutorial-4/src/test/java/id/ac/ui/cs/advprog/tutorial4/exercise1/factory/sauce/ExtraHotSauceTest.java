package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ExtraHotSauceTest {
    private ExtraHotSauce extraHotSauce;

    @Before
    public void setUp() {
        extraHotSauce = new ExtraHotSauce();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Extra Hot Sauce", extraHotSauce.toString());
    }
}