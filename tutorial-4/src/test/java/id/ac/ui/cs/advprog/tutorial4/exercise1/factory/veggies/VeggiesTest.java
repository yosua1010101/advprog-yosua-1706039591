package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;


public class VeggiesTest {
    private BlackOlives blackOlives;
    private Eggplant eggplant;
    private Garlic garlic;
    private Mushroom mushroom;
    private Onion onion;
    private RedPepper redPepper;
    private Spinach spinach;
    private Tomato tomato;


    @Before
    public void setUp() {
        blackOlives = new BlackOlives();
        eggplant = new Eggplant();
        garlic = new Garlic();
        mushroom = new Mushroom();
        onion = new Onion();
        redPepper = new RedPepper();
        spinach = new Spinach();
        tomato = new Tomato();
    }

    @Test
    public void testMethodToString() {
        Veggies[] actual = {blackOlives, eggplant, garlic, mushroom, onion, redPepper, spinach, tomato};
        String[] expected = {"Black Olives", "Eggplant", "Garlic", "Mushrooms", "Onion", "Red Pepper", "Spinach", "Tomato"};
        for (int i = 0; i < actual.length; i++) {
            assertEquals(expected[i], actual[i].toString());
        }
    }
}