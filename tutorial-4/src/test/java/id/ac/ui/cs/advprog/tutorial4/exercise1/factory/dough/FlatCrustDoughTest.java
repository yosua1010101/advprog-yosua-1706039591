package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class FlatCrustDoughTest {
    private FlatCrustDough flatCrustDough;

    @Before
    public void setUp() {
        flatCrustDough = new FlatCrustDough();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Flat Crust Dough", flatCrustDough.toString());
    }
}