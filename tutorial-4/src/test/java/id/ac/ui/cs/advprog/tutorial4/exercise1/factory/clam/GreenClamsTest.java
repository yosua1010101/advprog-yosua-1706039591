package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class GreenClamsTest {
    private GreenClams greenClams;

    @Before
    public void setUp() {
        greenClams = new GreenClams();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Green Clams from Java North Coast", greenClams.toString());
    }
}