package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class FlatCrustDough implements Dough {
    public String toString() {
        return "Flat Crust Dough";
    }
}
