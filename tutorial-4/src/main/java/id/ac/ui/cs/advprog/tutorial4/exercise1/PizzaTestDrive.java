package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

public class PizzaTestDrive {

    public static void main(String[] args) {
        PizzaStore nyStore = new NewYorkPizzaStore();

        Pizza pizza = nyStore.orderPizza("cheese");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = nyStore.orderPizza("clam");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = nyStore.orderPizza("veggie");
        System.out.println("Ethan ordered a " + pizza + "\n");

        PizzaStore deStore = new DepokPizzaStore();

        Pizza pizza2 = deStore.orderPizza("cheese");
        System.out.println("Yosua ordered a " + pizza2 + "\n");

        pizza2 = deStore.orderPizza("clam");
        System.out.println("Yosua ordered a " + pizza2 + "\n");

        pizza2 = deStore.orderPizza("veggie");
        System.out.println("Yosua ordered a " + pizza2 + "\n");
    }
}
