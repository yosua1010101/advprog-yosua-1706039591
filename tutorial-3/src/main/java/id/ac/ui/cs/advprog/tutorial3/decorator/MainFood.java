package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class MainFood {

    public static Food krabbyPatty;
    public static Food mySandwich;

    public static void main(String[] args) {
        krabbyPatty = BreadProducer.THICK_BUN.createBreadToBeFilled();
        krabbyPatty = FillingDecorator.BEEF_MEAT.addFillingToBread(krabbyPatty);
        krabbyPatty = FillingDecorator.CHEESE.addFillingToBread(krabbyPatty);
        krabbyPatty = FillingDecorator.CHILI_SAUCE.addFillingToBread(krabbyPatty);
        krabbyPatty = FillingDecorator.LETTUCE.addFillingToBread(krabbyPatty);
        krabbyPatty = FillingDecorator.TOMATO.addFillingToBread(krabbyPatty);

        mySandwich = BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled();
        mySandwich = FillingDecorator.CHICKEN_MEAT.addFillingToBread(mySandwich);
        mySandwich = FillingDecorator.CUCUMBER.addFillingToBread(mySandwich);
        mySandwich = FillingDecorator.TOMATO_SAUCE.addFillingToBread(mySandwich);

        System.out.println(krabbyPatty.getDescription() + ", becoming Krabby Patty");
        System.out.println(mySandwich.getDescription() + ", becoming My Sandwich");
    }
}