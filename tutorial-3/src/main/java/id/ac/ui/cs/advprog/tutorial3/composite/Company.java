package id.ac.ui.cs.advprog.tutorial3.composite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Company {
    protected List<Employees> employeesList;

    public Company() {
        employeesList = new ArrayList<Employees>();
    }

    public Company(List<Employees> employeesList) {
        Collections.copy(this.employeesList, employeesList);
    }

    public void addEmployee(Employees employees) {
        employeesList.add(employees);
    }

    public double getNetSalaries() {
        return employeesList.stream().mapToDouble(Employees::getSalary).sum();
    }

    public List<Employees> getAllEmployees() {
        return employeesList;
    }

    public String toString() {
        String result = "";
        for (Employees emp : employeesList) {
            result += "Name = " + emp.getName() + "\nRole = " + emp.getRole() + "\nSalary = " + emp.getSalary() + "\n==========\n";
        }
        result += "Total Salaries = " + this.getNetSalaries();
        return result;
    }
}
