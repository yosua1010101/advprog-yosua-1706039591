package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Tomato extends Food {
    Food food;

    public Tomato(Food food) {
        this.food = food;
        super.description = food.getDescription() + ", adding tomato";
    }

    @Override
    public String getDescription() {
        return super.description;
    }

    @Override
    public double cost() {
        return food.cost() + 0.50;
    }
}