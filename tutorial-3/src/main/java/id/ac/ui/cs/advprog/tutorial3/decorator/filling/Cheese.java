package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cheese extends Food {
    Food food;

    public Cheese(Food food) {
        this.food = food;
        super.description = food.getDescription() + ", adding cheese";
    }

    @Override
    public String getDescription() {
        return super.description;
    }

    @Override
    public double cost() {
        return food.cost() + 2.00;
    }
}