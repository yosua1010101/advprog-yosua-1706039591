package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChickenMeat extends Food {
    Food food;

    public ChickenMeat(Food food) {
        this.food = food;
        super.description = food.getDescription() + ", adding chicken meat";
    }

    @Override
    public String getDescription() {
        return super.description;
    }

    @Override
    public double cost() {
        return food.cost() + 4.50;
    }
}