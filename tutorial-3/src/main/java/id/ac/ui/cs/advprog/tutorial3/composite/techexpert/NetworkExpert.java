package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
//import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class NetworkExpert extends Employees {
    public NetworkExpert(String name, double salary) {
        super.name = name;
        if (salary < 50000.00) {
            throw new IllegalArgumentException();
        }
        super.salary = salary;
        super.role = "Network Expert";
    }

    @Override
    public double getSalary() {
        return super.salary;
    }
}
