package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
//import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class UiUxDesigner extends Employees {
    public UiUxDesigner(String name, double salary) {
        super.name = name;
        if (salary < 90000.00) {
            throw new IllegalArgumentException();
        }
        super.salary = salary;
        super.role = "UI/UX Designer";
    }

    @Override
    public double getSalary() {
        return super.salary;
    }
}
