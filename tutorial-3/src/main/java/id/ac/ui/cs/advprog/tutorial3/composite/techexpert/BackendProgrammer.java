package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import java.lang.IllegalArgumentException;

public class BackendProgrammer extends Employees {
    public BackendProgrammer(String name, double salary) {
        super.name = name;
        if (salary < 20000.00) {
            throw new IllegalArgumentException();
        }
        super.salary = salary;
        super.role = "Back End Programmer";
    }

    @Override
    public double getSalary() {
        return super.salary;
    }
}
