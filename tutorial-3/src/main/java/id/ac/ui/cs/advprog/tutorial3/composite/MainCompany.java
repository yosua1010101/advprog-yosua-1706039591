package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.Company;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.*;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

public class MainCompany {

    public static Company bts;
    public static Ceo namjoon;
    public static Cto seokjin;
    public static BackendProgrammer yoongi;
    public static FrontendProgrammer hoseok;
    public static NetworkExpert jimin;
    public static SecurityExpert taehyung;
    public static UiUxDesigner jungkook;

    public static void main(String[] args) {
        
        bts = new Company();
        namjoon = new Ceo("Kim Namjoon", 200000.00);
        seokjin = new Cto("Kim Seokjin", 100000.00);
        yoongi = new BackendProgrammer("Min Yoongi", 20000.00);
        hoseok = new FrontendProgrammer("Jung Hoseok", 30000.00);
        jimin = new NetworkExpert("Park Jimin", 50000.00);
        taehyung = new SecurityExpert("Kim Taehyung", 70000.00);
        jungkook = new UiUxDesigner("Jeon Jungkook", 90000.00);

        bts.addEmployee(namjoon);
        bts.addEmployee(seokjin);
        bts.addEmployee(yoongi);
        bts.addEmployee(hoseok);
        bts.addEmployee(jimin);
        bts.addEmployee(taehyung);
        bts.addEmployee(jungkook);

        System.out.println(bts.toString());
    }
}